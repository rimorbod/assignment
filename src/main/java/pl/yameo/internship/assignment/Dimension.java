package pl.yameo.internship.assignment;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.security.InvalidParameterException;

@Data
@AllArgsConstructor
public class Dimension {
    private String name;
    private Double value;

    public void setValue(Double value) {
        if (value > 0) {
            this.value = value;
        } else {
            throw new InvalidParameterException("Dimension value have to be greater than zero!");
        }
    }

    @Override
    public String toString() {
        return name + " = " + value;
    }
}
