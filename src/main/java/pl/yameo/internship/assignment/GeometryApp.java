package pl.yameo.internship.assignment;

import pl.yameo.internship.assignment.shapeModels.Shape;
import pl.yameo.internship.assignment.shapeServices.ShapeCreator;
import pl.yameo.internship.assignment.shapeServices.ShapeCreatorImpl;
import pl.yameo.internship.assignment.shapeServices.ShapeListProvider;
import pl.yameo.internship.assignment.shapeServices.ShapeListProviderImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GeometryApp {
    private Scanner scanner;
    private List<Shape> shapes = new ArrayList<>();
    private ShapeListProvider provider;
    private ShapeCreator creator;

    public GeometryApp(Scanner scanner) {
        this.scanner = scanner;
    }

    public void start() {
        provider = new ShapeListProviderImpl();
        creator = new ShapeCreatorImpl();
        boolean run = true;
        while (run) {
            run = run();
        }
    }

    private boolean run() {
        System.out.println("Choose action:");
        System.out.println("1) Create new shape");
        System.out.println("2) List existing shapes");
        System.out.println("3) Modify one of the shapes from the list");
        System.out.println("0) Exit");

        int option = readInteger();
        switch (option) {
            case 0:
                return false;
            case 1:
                Shape newShape = createNewShape();
                addNewShapeIfNotNull(newShape);
                break;
            case 2:
                listShapes();
                break;
            case 3:
                if (shapes.isEmpty()) {
                    System.out.println("You have nothing to edit, create new shape first");
                } else {
                    modifyShape();
                }
                break;
            default:
                System.out.println("You must choose a valid option, try again");
                break;

        }

        return true;
    }

    private Shape createNewShape() {
        int index = 1;
        int option = -1;
        while ((option < 0) || (option > provider.getShapeClassesList().size())) {
            System.out.println("Choose shape to create:");
            for (Class c : provider.getShapeClassesList()) {
                System.out.println(index + ") " + c.getSimpleName());
                index++;
            }
            System.out.println("0) Back");
            option = readInteger();
        }

        if (option > 0) {
            try {
                return creator.create(provider.getShapeClassesList().get(option - 1));
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return null;
            }
        } else {
            System.out.println("Shape wasn't created, try again");
            return null;
        }

    }

    private void listShapes() {
        System.out.println("====== LIST OF SHAPES ======");
        shapes.forEach(shape -> shapeInfoOutput(shape));
        System.out.println("============================");
    }

    private void modifyShape() {
        listShapes();
        int index = 0;
        while ((index < 1) || (index > shapes.size())) {
            System.out.println("Please choose the index of the shape you want to modify (1-" + shapes.size() + "): ");
            index = readInteger();
        }
        int position = index - 1;
        Shape activeShape = shapes.get(position);

        shapeInfoOutput(activeShape);

        Shape newShape = null;
        try {
            newShape = creator.create(activeShape.getClass());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (newShape != null) {
            System.out.println("Old shape: ");
            shapeInfoOutput(activeShape);
            System.out.println("============================");
            System.out.println("New shape: ");
            shapeInfoOutput(newShape);
            System.out.println("============================");
            shapes.set(position, newShape);
        } else {
            System.out.println("Edition failed, please try again");
        }
    }

    private Integer readInteger() {
        Integer value = null;
        while (value == null) {
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
            } else {
                scanner.next();
            }
        }
        return value;
    }

    private void shapeInfoOutput(Shape shape) {
        System.out.print(shape.getName() + " with dimensions: ");
        System.out.print(shape.listDimensions() + "; ");
        System.out.print("Area: " + shape.calculateArea() + "; ");
        System.out.println("Perimeter: " + shape.calculatePerimeter());
    }

    public void addNewShapeIfNotNull(Shape newShape) {
        if (newShape != null) {
            shapes.add(newShape);
        }
    }
}
