package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.util.Arrays;
import java.util.List;

public class Circle implements Shape {

    private Dimension radius = new Dimension("radius", 0.0);

    private Circle() {
    }

    public Circle(Double radius) {
        setRadius(radius);
    }

    @Override
    public String getName() {
        return "Circle";
    }

    @Override
    public List<Dimension> listDimensions() {
        return Arrays.asList(radius);
    }

    @Override
    public Double calculateArea() {
        return new Ellipse(radius.getValue(), radius.getValue()).calculateArea();
    }

    @Override
    public Double calculatePerimeter() {
        return new Ellipse(radius.getValue(), radius.getValue()).calculatePerimeter();
    }

    @Override
    public String askForCreation() {
        return "Please provide the radius for the circle:";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        setRadius(dimensionsList.get(0));
    }

    public void setRadius(Double dimension) {
        radius.setValue(dimension);
    }

}
