package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;

public class Triangle implements Shape {
    private Dimension edgeA = new Dimension("edgeA", 0.0);
    private Dimension edgeB = new Dimension("edgeB", 0.0);
    private Dimension edgeC = new Dimension("edgeC", 0.0);

    private Triangle() {
    }

    public Triangle(Double edgeA, Double edgeB, Double edgeC) {
        if (edgeValidation(edgeA, edgeB, edgeC)) {
            throw new InvalidParameterException("Triangle with provided edges couldn't be created");
        } else {
            setAllParams(Arrays.asList(edgeA, edgeB, edgeC));
        }
    }

    @Override
    public String getName() {
        return "Triangle";
    }

    @Override
    public final List<Dimension> listDimensions() {
        return Arrays.asList(edgeA, edgeB, edgeC);
    }

    @Override
    public final Double calculateArea() {
        Double halfPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - edgeA.getValue()) * (halfPerimeter - edgeB.getValue()) * (halfPerimeter - edgeC.getValue()));
    }

    @Override
    public final Double calculatePerimeter() {
        return edgeA.getValue() + edgeB.getValue() + edgeC.getValue();
    }

    @Override
    public String askForCreation() {
        return "Please provide three edge lengths:";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        if (edgeValidation(dimensionsList.get(0), dimensionsList.get(1), dimensionsList.get(2))) {
            throw new InvalidParameterException("Triangle with provided edges couldn't be created");
        }
        setEdgeA(dimensionsList.get(0));
        setEdgeB(dimensionsList.get(1));
        setEdgeC(dimensionsList.get(2));
    }

    public void setEdgeA(Double edgeA) {
        this.edgeA.setValue(edgeA);
    }

    public void setEdgeB(Double edgeB) {
        this.edgeB.setValue(edgeB);
    }

    public void setEdgeC(Double edgeC) {
        this.edgeC.setValue(edgeC);
    }

    private boolean edgeValidation(Double edgeA, Double edgeB, Double edgeC) {
        return ((edgeA + edgeB) <= edgeC) || ((edgeA + edgeC) <= edgeB) || ((edgeB + edgeC) <= edgeA);
    }
}
