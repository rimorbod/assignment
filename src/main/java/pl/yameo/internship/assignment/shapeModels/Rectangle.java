package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.util.Arrays;
import java.util.List;

public class Rectangle implements Shape {
    private Dimension height = new Dimension("height", 0.0);
    private Dimension width = new Dimension("width", 0.0);

    private Rectangle() {
    }

    public Rectangle(Double height, Double width) {
        setAllParams(Arrays.asList(height, width));
    }

    @Override
    public String getName() {
        return "Rectangle";
    }

    @Override
    public final List<Dimension> listDimensions() {
        return Arrays.asList(height, width);
    }

    @Override
    public final Double calculateArea() {
        return height.getValue() * width.getValue();
    }

    @Override
    public final Double calculatePerimeter() {
        return 2 * (height.getValue() + width.getValue());
    }

    @Override
    public String askForCreation() {
        return "Please provide two edge lengths (height, width):";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        setHeight(dimensionsList.get(0));
        setWidth(dimensionsList.get(1));
    }

    public void setHeight(Double height) {
        this.height.setValue(height);
    }

    public void setWidth(Double width) {
        this.width.setValue(width);
    }
}
