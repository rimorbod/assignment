package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.util.Arrays;
import java.util.List;

public class Ellipse implements Shape {
    private Dimension semiMajorAxis = new Dimension("semi major axis", 0.0);
    private Dimension semiMinorAxis = new Dimension("semi minor axis", 0.0);

    private Ellipse() {
    }

    public Ellipse(Double semiMajorAxis, Double semiMinorAxis) {
        setAllParams(Arrays.asList(semiMajorAxis, semiMinorAxis));
    }

    @Override
    public String getName() {
        return "Ellipse";
    }

    @Override
    public final List<Dimension> listDimensions() {
        return Arrays.asList(semiMajorAxis, semiMinorAxis);
    }

    @Override
    public final Double calculateArea() {
        return Math.PI * semiMajorAxis.getValue() * semiMinorAxis.getValue();
    }

    @Override
    public final Double calculatePerimeter() {
        return Math.PI * (3 * (semiMajorAxis.getValue() + semiMinorAxis.getValue()) / 2 - Math.sqrt(semiMajorAxis.getValue() * semiMinorAxis.getValue()));
    }

    @Override
    public String askForCreation() {
        return "Please provide two semi-axis lengths (major, minor):";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        setSemiMajorAxis(dimensionsList.get(0));
        setSemiMinorAxis(dimensionsList.get(1));
    }

    public void setSemiMajorAxis(Double semiMajorAxis) {
        this.semiMajorAxis.setValue(semiMajorAxis);
    }

    public void setSemiMinorAxis(Double semiMinorAxis) {
        this.semiMinorAxis.setValue(semiMinorAxis);
    }

}
