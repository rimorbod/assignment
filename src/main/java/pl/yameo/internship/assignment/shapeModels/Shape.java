package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.util.List;

/**
 * All shapes have to have a private non-args constructor.
 * All dimensions used by shape have to have their names settled, and values initialized to work properly with actual services.
 */
public interface Shape {

    String getName();

    List<Dimension> listDimensions();

    Double calculateArea();

    Double calculatePerimeter();

    /**
     * This is a method to ask user to provide all necessary values for shape dimensions.
     *
     */
    String askForCreation();

    /**
     * This method is used by application to set all values of dimensions from the list, provided f.eg. from user.
     *
     */
    void setAllParams(List<Double> dimensionsList);
}
