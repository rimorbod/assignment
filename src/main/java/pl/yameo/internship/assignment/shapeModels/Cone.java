package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.List;

public class Cone implements Shape {

    private Dimension radius = new Dimension("radius", 0.0);
    private Dimension angleMeasure = new Dimension("angle measure", 0.0);

    private Cone() {
    }

    public Cone(Double radius, Double angleMeasure) {
        setRadius(radius);
        setAngleMeasure(angleMeasure);
    }

    @Override
    public String getName() {
        return "Cone";
    }

    @Override
    public List<Dimension> listDimensions() {
        return Arrays.asList(radius, angleMeasure);
    }

    @Override
    public Double calculateArea() {
        return new Circle(radius.getValue()).calculateArea() * (angleMeasure.getValue() / (2 * Math.PI));
    }

    @Override
    public Double calculatePerimeter() {
        return 2 * radius.getValue() + (new Circle(radius.getValue()).calculatePerimeter()) * (angleMeasure.getValue() / (2 * Math.PI));
    }

    @Override
    public String askForCreation() {
        return "Please provide the radius and the angle measure in radians (0-2π) for the cone:";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        setRadius(dimensionsList.get(0));
        setAngleMeasure(dimensionsList.get(1));
    }

    public void setRadius(Double radius) {
        this.radius.setValue(radius);
    }

    public void setAngleMeasure(Double angleMeasure) {
        if ((angleMeasure > 0) && (angleMeasure < 2 * Math.PI)) {
            this.angleMeasure.setValue(angleMeasure);
        } else {
            throw new InvalidParameterException("Cone with provided angle measure couldn't be created");
        }
    }
}
