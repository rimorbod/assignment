package pl.yameo.internship.assignment.shapeModels;

import pl.yameo.internship.assignment.Dimension;

import java.util.Arrays;
import java.util.List;

public class Square implements Shape {

    private Dimension edge = new Dimension("edge", 0.0);

    private Square() {
    }

    public Square(Double edge) {
        this.edge.setValue(edge);
    }

    @Override
    public String getName() {
        return "Square";
    }

    @Override
    public List<Dimension> listDimensions() {
        return Arrays.asList(edge);
    }

    @Override
    public Double calculateArea() {
        return new Rectangle(edge.getValue(), edge.getValue()).calculateArea();
    }

    @Override
    public Double calculatePerimeter() {
        return new Rectangle(edge.getValue(), edge.getValue()).calculatePerimeter();
    }

    @Override
    public String askForCreation() {
        return "Please provide the edge length:";
    }

    @Override
    public void setAllParams(List<Double> dimensionsList) {
        setEdge(dimensionsList.get(0));
    }


    public void setEdge(Double dimension) {
        this.edge.setValue(dimension);
    }

}
