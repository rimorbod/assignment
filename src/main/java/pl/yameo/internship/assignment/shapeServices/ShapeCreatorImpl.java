package pl.yameo.internship.assignment.shapeServices;

import pl.yameo.internship.assignment.shapeModels.Shape;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ShapeCreatorImpl implements ShapeCreator {

    private ShapeParamsProvider provider;

    public ShapeCreatorImpl() {
        provider = new ShapeParamsProviderImpl();
    }

    public Shape create(Class<? extends Shape> shapeClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Shape shape;
        Constructor<? extends Shape> constructor = shapeClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        shape = constructor.newInstance();
        provider.setParameters(shape);
        return shape;
    }
}
