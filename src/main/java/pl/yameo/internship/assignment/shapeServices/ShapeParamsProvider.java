package pl.yameo.internship.assignment.shapeServices;

import pl.yameo.internship.assignment.shapeModels.Shape;

public interface ShapeParamsProvider {
    /**
     * This method is used to fill Shape object with proper Dimension values.
     * Shape object is delivered to method and changed inside.
     * In our case Dimension values are taken from user.
     **/
    void setParameters(Shape shape);
}
