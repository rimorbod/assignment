package pl.yameo.internship.assignment.shapeServices;

import pl.yameo.internship.assignment.shapeModels.Shape;

import java.util.List;

public interface ShapeListProvider {
    /**
     * This method is used to provide list of Classes that implements Shape for the application.
     * Used to create menu options while communicating with user.
     */
    List<Class<? extends Shape>> getShapeClassesList();
}
