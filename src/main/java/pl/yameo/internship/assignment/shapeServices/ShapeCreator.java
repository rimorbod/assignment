package pl.yameo.internship.assignment.shapeServices;

import pl.yameo.internship.assignment.shapeModels.Shape;

public interface ShapeCreator {
    /**
     * This is a method to make an instance of a class implementing Shape, based at Class provided.
     * Specific Exceptions depends on implementation.
     */
    Shape create(Class<? extends Shape> shapeClass) throws Exception;
}
