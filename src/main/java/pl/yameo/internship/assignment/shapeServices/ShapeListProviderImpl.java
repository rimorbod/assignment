package pl.yameo.internship.assignment.shapeServices;

import org.reflections.Reflections;
import pl.yameo.internship.assignment.shapeModels.Shape;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ShapeListProviderImpl implements ShapeListProvider {

    private List<Class<? extends Shape>> shapeClassesList = new ArrayList<>();

    public ShapeListProviderImpl() {
        loadClasses();
    }

    public List<Class<? extends Shape>> getShapeClassesList() {
        return shapeClassesList;
    }

    private void loadClasses() {
        Reflections reflections = new Reflections("pl.yameo.internship.assignment.shapeModels");
        Set<Class<? extends Shape>> classes = reflections.getSubTypesOf(Shape.class);
        shapeClassesList.addAll(classes);
    }
}
