package pl.yameo.internship.assignment.shapeServices;

import pl.yameo.internship.assignment.shapeModels.Shape;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShapeParamsProviderImpl implements ShapeParamsProvider {

    private Scanner scanner;

    public ShapeParamsProviderImpl() {
        scanner = new Scanner(System.in);
    }

    public void setParameters(Shape shape) {
        System.out.println(shape.askForCreation());
        List<Double> parameters = new ArrayList<>();
        int dimensionsNumber = shape.listDimensions().size();
        while (dimensionsNumber > 0) {
            parameters.add(readPositiveDouble());
            dimensionsNumber--;
        }
        shape.setAllParams(parameters);
    }

    private Double readPositiveDouble() {
        Double value = null;
        while (value == null) {
            if (scanner.hasNextDouble()) {
                value = scanner.nextDouble();
                if (value <= 0) {
                    System.out.println("Number have to be larger than 0");
                    return readPositiveDouble();
                }
            } else {
                scanner.next();
            }
        }
        return value;
    }
}
