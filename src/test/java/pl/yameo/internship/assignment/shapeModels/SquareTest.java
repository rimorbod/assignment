package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    private static final double DELTA = 0.001;
    private static final double INITIAL_DIMENSION = 4.0;
    private static final String SHAPE_NAME = "Square";
    private static final double NEW_EDGE_LENGTH = 5.0;
    private static final double INITIAL_AREA = 16.0;
    private static final double INITIAL_PERIMETER = 16.0;

    Square square;

    @Before
    public void setup() {
        square = new Square(INITIAL_DIMENSION);
    }

    @Test
    public void when_square_created_then_it_has_proper_name() {
        assertEquals(square.getName(), SHAPE_NAME);
    }

    @Test
    public void when_edge_set_then_new_dimension_is_returned() {
        square.setEdge(NEW_EDGE_LENGTH);
        assertEquals(square.listDimensions().get(0).getValue(), NEW_EDGE_LENGTH, DELTA);
    }

    @Test
    public void when_square_width_is_halved_then_its_area_is_quartered() {
        assertEquals(square.calculateArea(), INITIAL_AREA, DELTA);
        square.setEdge(INITIAL_DIMENSION / 2);
        assertEquals(square.calculateArea(), INITIAL_AREA / 4, DELTA);
    }

    @Test
    public void when_calculatePerimeter_called_then_should_return_proper_value() {
        assertEquals(square.calculatePerimeter(), INITIAL_PERIMETER, DELTA);
    }

    @Test
    public void when_calculateArea_called_then_should_return_proper_value() {
        assertEquals(square.calculateArea(), INITIAL_AREA, DELTA);
    }
}
