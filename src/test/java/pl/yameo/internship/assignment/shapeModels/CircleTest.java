package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    private static final double DELTA = 0.001;
    private static final double INITIAL_DIMENSION = 7.0;
    private static final String SHAPE_NAME = "Circle";
    private static final double NEW_DIMENSION_VALUE = 5.0;

    Circle circle;

    @Before
    public void setup() {
        circle = new Circle(INITIAL_DIMENSION);
    }

    @Test
    public void when_circle_created_then_it_has_proper_name() {
        assertEquals(circle.getName(), SHAPE_NAME);
    }

    @Test
    public void when_radius_set_then_new_dimension_is_returned() {
        circle.setRadius(NEW_DIMENSION_VALUE);
        assertEquals(NEW_DIMENSION_VALUE, circle.listDimensions().get(0).getValue(), DELTA);
    }

    @Test
    public void when_calculateArea_called_then_proper_value_is_returned() {
        assertEquals(INITIAL_DIMENSION * INITIAL_DIMENSION * Math.PI, circle.calculateArea(), DELTA);
    }

    @Test
    public void when_calculatePerimeter_called_then_proper_value_is_returned() {
        assertEquals(INITIAL_DIMENSION * 2 * Math.PI, circle.calculatePerimeter(), DELTA);
    }
}