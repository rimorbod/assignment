package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConeTest {

    private static final double DELTA = 0.001;
    private static final double INITIAL_RADIUS = 4.0;
    private static final double INITIAL_ANGLE_MEASURE = Math.PI / 2;
    private static final String SHAPE_NAME = "Cone";
    private static final double NEW_RADIUS_VALUE = 5.0;
    private static final double NEW_ANGLE_MEASURE = Math.PI;

    Cone cone;

    @Before
    public void setup() {
        cone = new Cone(INITIAL_RADIUS, INITIAL_ANGLE_MEASURE);
    }

    @Test
    public void when_cone_created_then_it_has_proper_name() {
        assertEquals(cone.getName(), SHAPE_NAME);
    }

    @Test
    public void when_radius_set_then_dimension_is_changed() {
        cone.setRadius(NEW_RADIUS_VALUE);
        assertEquals(NEW_RADIUS_VALUE, cone.listDimensions().get(0).getValue(), DELTA);
    }

    @Test
    public void when_angle_measure_set_then_dimension_is_changed() {
        cone.setAngleMeasure(NEW_ANGLE_MEASURE);
        assertEquals(NEW_ANGLE_MEASURE, cone.listDimensions().get(1).getValue(), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_inappropriate_angle_measure_provided() {
        cone.setAngleMeasure(Math.PI * 2);
    }

    @Test
    public void when_calculateArea_called_then_should_return_proper_value() {
        assertEquals(new Circle(INITIAL_RADIUS).calculateArea() / 4, cone.calculateArea(), DELTA);
    }
}