package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class TriangleTest {
    private static final Double initialEdgeA = 3.0;
    private static final Double initialEdgeB = 4.0;
    private static final Double initialEdgeC = 5.0;

    private Triangle triangle;

    @Before
    public void setup() {
        triangle = new Triangle(initialEdgeA, initialEdgeB, initialEdgeC);
    }

    @Test
    public void when_triangle_is_created_then_proper_dimensions_are_returned() {

        assertEquals(initialEdgeA, triangle.listDimensions().get(0).getValue(), 0.0001);
        assertEquals(initialEdgeB, triangle.listDimensions().get(1).getValue(), 0.0001);
        assertEquals(initialEdgeC, triangle.listDimensions().get(2).getValue(), 0.0001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_impossible_triangle_is_created_then_exception_is_thrown() {
        new Triangle(initialEdgeA, 1.0, initialEdgeC);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_impossible_triangle_is_created_by_edition_then_exception_is_thrown() {
        triangle.setAllParams(Arrays.asList(initialEdgeA, 1.0, initialEdgeC));
    }

    @Test
    public void when_triangle_is_edited_then_proper_dimensions_are_returned() {

        triangle.setAllParams(Arrays.asList(initialEdgeB, initialEdgeB, initialEdgeB));

        assertEquals(initialEdgeB, triangle.listDimensions().get(0).getValue(), 0.0001);
        assertEquals(initialEdgeB, triangle.listDimensions().get(1).getValue(), 0.0001);
        assertEquals(initialEdgeB, triangle.listDimensions().get(2).getValue(), 0.0001);
    }

}
