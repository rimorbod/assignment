package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

    private static final double DELTA = 0.001;
    private static final double INITIAL_HEIGHT = 4.0;
    private static final double INITIAL_WIDTH = 6.0;
    private static final String SHAPE_NAME = "Rectangle";
    private static final double NEW_EDGE_LENGTH = 5.0;
    private static final double INITIAL_AREA = 24.0;


    private Rectangle rectangle;

    @Before
    public void setup() {
        rectangle = new Rectangle(INITIAL_HEIGHT, INITIAL_WIDTH);
    }

    @Test
    public void when_rectangle_created_then_it_has_proper_name() {
        assertEquals(rectangle.getName(), SHAPE_NAME);
    }

    @Test
    public void when_width_set_then_dimension_is_changed() {
        rectangle.setWidth(NEW_EDGE_LENGTH);
        assertEquals(rectangle.listDimensions().get(1).getValue(), NEW_EDGE_LENGTH, DELTA);
    }

    @Test
    public void when_height_set_then_dimension_is_changed() {
        rectangle.setHeight(NEW_EDGE_LENGTH);
        assertEquals(rectangle.listDimensions().get(0).getValue(), NEW_EDGE_LENGTH, DELTA);
    }

    @Test
    public void when_rectangle_height_is_halved_then_its_area_is_halved() {
        rectangle.setHeight(INITIAL_HEIGHT / 2);
        assertEquals(rectangle.calculateArea(), INITIAL_AREA / 2, DELTA);
    }

    @Test
    public void when_rectangle_is_edited_then_proper_dimensions_are_returned() {
        rectangle.setAllParams(Arrays.asList(NEW_EDGE_LENGTH, NEW_EDGE_LENGTH));
        assertEquals(rectangle.listDimensions().get(0).getValue(), NEW_EDGE_LENGTH, DELTA);
        assertEquals(rectangle.listDimensions().get(1).getValue(), NEW_EDGE_LENGTH, DELTA);
    }
}
