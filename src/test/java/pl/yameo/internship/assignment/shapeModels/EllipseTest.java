package pl.yameo.internship.assignment.shapeModels;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class EllipseTest {

    private static final double DELTA = 0.001;
    private static final double INITIAL_SEMI_MINOR_AXIS = 4.0;
    private static final double INITIAL_SEMI_MAJOR_AXIS = 6.0;
    private static final String SHAPE_NAME = "Ellipse";
    private static final double NEW_DIMENSION_VALUE = 5.0;

    private Ellipse ellipse;

    @Before
    public void setup() {
        ellipse = new Ellipse(INITIAL_SEMI_MAJOR_AXIS, INITIAL_SEMI_MINOR_AXIS);
    }

    @Test
    public void when_ellipse_created_then_it_has_proper_name() {
        assertEquals(ellipse.getName(), SHAPE_NAME);
    }

    @Test
    public void when_ellipse_is_edited_then_proper_dimensions_are_returned() {
        ellipse.setAllParams(Arrays.asList(NEW_DIMENSION_VALUE, NEW_DIMENSION_VALUE));
        assertEquals(ellipse.listDimensions().get(0).getValue(), NEW_DIMENSION_VALUE, DELTA);
        assertEquals(ellipse.listDimensions().get(1).getValue(), NEW_DIMENSION_VALUE, DELTA);
    }
}