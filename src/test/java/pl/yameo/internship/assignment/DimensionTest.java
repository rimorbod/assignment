package pl.yameo.internship.assignment;

import org.junit.Test;

public class DimensionTest {

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_inappropriate_value_is_set() {
        Dimension dimension = new Dimension("dimension", 5.5);
        dimension.setValue(0.0);
    }
}