package pl.yameo.internship.assignment.shapeServices;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.yameo.internship.assignment.shapeModels.*;

import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class ShapeListProviderImplTest {

    public Object paramsForClassTest() {
        return new Object[][]{
                {Circle.class},
                {Square.class},
                {Rectangle.class},
                {Ellipse.class},
                {Cone.class},
                {Triangle.class}
        };
    }

    @Test
    @Parameters(method = "paramsForClassTest")
    public void when_getShapeClassesList_called_then_should_contains_proper_classes(Class clazz) {
        ShapeListProvider provider = new ShapeListProviderImpl();
        assertTrue(provider.getShapeClassesList().contains(clazz));
    }

    @Test
    public void when_provider_instantiated_then_list_should_not_be_empty() {
        ShapeListProviderImpl provider = new ShapeListProviderImpl();
        assertTrue(!provider.getShapeClassesList().isEmpty());
    }
}